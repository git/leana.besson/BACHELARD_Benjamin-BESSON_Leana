[![Build Status](https://codefirst.iut.uca.fr/api/badges/leana.besson/BACHELARD_Benjamin-BESSON_Leana/status.svg)](https://codefirst.iut.uca.fr/leana.besson/BACHELARD_Benjamin-BESSON_Leana)  
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=alert_status)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=bugs)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=code_smells)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=coverage)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)  
[![Duplicated Lines (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=duplicated_lines_density)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Lines of Code](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=ncloc)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=sqale_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=reliability_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)  
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=security_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Technical Debt](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=sqale_index)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BACHELARD_Benjamin-BESSON_Leana&metric=vulnerabilities)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BACHELARD_Benjamin-BESSON_Leana)  

 
# BACHELARD_Benjamin-BESSON_Leana

Welcome on the BACHELARD_Benjamin-BESSON_Leana project!  

  

_Generated with a_ **Code#0** _template_  
<img src="Documentation/doc_images/CodeFirst.png" height=40/>   