using System.Runtime.CompilerServices;
using Model;
using NuGet.Frameworks;

namespace Tests
{
    public class Tests_race
    {
        [Theory]
        [InlineData("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", null, null)]
        [InlineData("American curl", "American curl", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Un petit conseil", "Un petit conseil", null, null)]
        [InlineData("Abyssin", "Abyssin", "Felis catus", "Felis catus", "15 � 20 ans", "15 � 20 ans", "2.7 � 5.5 kg", "2.7 � 5.5 kg", "30 � 45 cm", "30 � 45 cm", "Son comportement", "Son comportement", "Sa sant�", "Sa sant�", "Son �ducation", "Son �ducation", "Son entretien", "Son entretien", "Son cout", "Son cout", "Un petit conseil", "Un petit conseil", "abyssin.png", "abyssin.png")]
        public void TestConstructor(string nom, string expectedNom, string nomScientifique, string exceptedNomScientifique, string esperanceVie, string exceptedEsperanceVie, string poidsMoyen, string exceptedPoidsMoyen, string tailleMoyenne, string exceptedTailleMoyenne, string comportement, string exceptedComportement, string sante, string exceptedSante, string education, string exceptedEducation, string entretien, string exceptedEntretien, string cout, string exceptedCout, string conseil, string exceptedConseil, string? image, string? exceptedImage)
        {
            Race r = new Race(nom, nomScientifique, esperanceVie, poidsMoyen, tailleMoyenne, comportement, sante, education, entretien, cout, conseil, image);
            Assert.Equal(expectedNom, r.Nom);
            Assert.Equal(exceptedNomScientifique, r.NomScientifique);
            Assert.Equal(exceptedEsperanceVie, r.EsperanceVie);
            Assert.Equal(exceptedPoidsMoyen, r.PoidsMoyen);
            Assert.Equal(exceptedTailleMoyenne, r.TailleMoyenne);
            Assert.Equal(exceptedComportement, r.Comportement);
            Assert.Equal(exceptedSante, r.Sante);
            Assert.Equal(exceptedEducation, r.Education);
            Assert.Equal(exceptedEntretien, r.Entretien);
            Assert.Equal(exceptedCout, r.Cout);
            Assert.Equal(exceptedConseil, r.Conseil);
            Assert.Equal(exceptedImage, r.Image);
        }
    }
}