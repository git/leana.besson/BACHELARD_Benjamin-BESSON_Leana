﻿using Model;

namespace Tests
{
    public class Test_espece
    {
        [Theory]
        public void TestConstructor(string nom, string expectedNom, string nomScientifique, string exceptedNomScientifique, string esperanceVie, string exceptedEsperanceVie, string poidsMoyen, string exceptedPoidsMoyen, string tailleMoyenne, string exceptedTailleMoyenne, string comportement, string exceptedComportement, string sante, string exceptedSante, string education, string exceptedEducation, string entretien, string exceptedEntretien, string cout, string exceptedCout, string conseil, string exceptedConseil, string? image, string? exceptedImage, List<Race> listeRace, List<Race> exceptedListeRace)
        {
            Espece e = new Espece(listeRace, nom, nomScientifique, image, esperanceVie, poidsMoyen, tailleMoyenne, comportement, sante, education, entretien, cout, conseil);
            Assert.Equal(expectedNom, e.Nom);
            Assert.Equal(exceptedNomScientifique, e.NomScientifique);
            Assert.Equal(exceptedEsperanceVie, e.EsperanceVie);
            Assert.Equal(exceptedPoidsMoyen, e.PoidsMoyen);
            Assert.Equal(exceptedTailleMoyenne, e.TailleMoyenne);
            Assert.Equal(exceptedComportement, e.Comportement);
            Assert.Equal(exceptedSante, e.Sante);
            Assert.Equal(exceptedEducation, e.Education);
            Assert.Equal(exceptedEntretien, e.Entretien);
            Assert.Equal(exceptedCout, e.Cout);
            Assert.Equal(exceptedConseil, e.Conseil);
            Assert.Equal(exceptedImage, e.Image);
            Assert.Equal(exceptedListeRace.Count, e.ListeRaces.Count());
            Assert.All(exceptedListeRace, r => e.ListeRaces.Contains(r));
        }
    }
}
