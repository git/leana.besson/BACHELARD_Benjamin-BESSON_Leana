﻿/*!
 * \file DataSerializerXML.cs
 * \author Léana Besson
 */
using System.Runtime.Serialization;
using System.Xml;
using Model;

/*!
 * \namespace Persistance
 */
namespace Persistance
{
    /*!
     * \class DataSerializerXML
     * \brief Contains all the information and functions needed to serialize information in XML format
     */
    public class DataSerializerXML
    {
        /*!
         * \fn Serializer(string path, Theque theque)
         * \brief Serializes theque information in the file theque.xml
         * \param path string - Path of the file in which the file is to be saved 
         * \param theque Theque - Theque to be serialized
         */
        public static void Serializer(string path, Theque theque)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string xmlFile = "theque.xml";

            var serializer = new DataContractSerializer(typeof(Theque), new DataContractSerializerSettings() { PreserveObjectReferences = true });

            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText(xmlFile))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, theque);
                }
            }
        }

        /*!
         * \fn Deserializer(string path)
         * \brief Deserializes the information contained in theque.xml file if it exists, otherwise it retrieves the information from the stub
         * \param path string - Path of the file in which the file is to be saved 
         * \return Theque - Theque with recovered data 
         */
        public static Theque Deserializer(string path)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string xmlFile = "theque.xml";

            var serializer = new DataContractSerializer(typeof(Theque), new DataContractSerializerSettings() { PreserveObjectReferences = true });

            Theque theque = new Theque();
            if(File.Exists(xmlFile))
            {
                using (Stream stream = File.OpenRead(xmlFile))
                {
                    Theque? thequeOpt = serializer.ReadObject(stream) as Theque;
                    if (thequeOpt != null)
                        theque = thequeOpt;
                    else
                        Console.WriteLine("Theque est null");
                }
            }
            else
            {
                theque = Stub.LoadTheque();
            }
            return theque;
        }
    }
}