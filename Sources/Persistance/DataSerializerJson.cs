﻿/*!
 * \file DataSerializerJson.cs
 * \author Léana Besson
 */using Model;
using System.Runtime.Serialization.Json;

/*!
 * \namespace Persistance
 */
namespace Persistance
{
    /*!
     * \class DataSerializerJson
     * \brief Contains all the information and functions needed to serialize information in Json format
     */
    public class DataSerializerJson
    {
        /*!
         * \fn Serializer(string path, Theque theque)
         * \brief Serializes theque information in the file theque.json
         * \param path string - Path of the file in which the file is to be saved 
         * \param theque Theque - Theque to be serialized
         */
        public static void Serializer(string path, Theque theque)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string jsonFile = "theque.json";

            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Theque));

            using (FileStream stream = File.Create(jsonFile))
            {
                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(stream, System.Text.Encoding.UTF8, false, true))
                {
                    jsonSerializer.WriteObject(writer, theque);
                }
            }
        }

        /*!
         * \fn Deserializer(string path)
         * \brief Deserializes the information contained in theque.json file if it exists, otherwise it retrieves the information from the stub
         * \param path string - Path of the file in which the file is to be saved 
         * \return Theque - Theque with recovered data 
         */
        public static Theque Deserializer(string path)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string jsonFile = "theque.json";

            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Theque));

            Theque? theque = new Theque();
            if (File.Exists(jsonFile))
            {
                using (Stream stream = File.OpenRead(jsonFile))
                {
                    Theque? thequeOpt = jsonSerializer.ReadObject(stream) as Theque;
                    if (thequeOpt != null)
                        theque = thequeOpt;
                    else
                        Console.WriteLine("Theque est null");
                }
            }
            else
            {
                theque = Stub.LoadTheque();
            }
            return theque;
        }   
    }
}
