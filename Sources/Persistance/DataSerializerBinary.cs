﻿/*!
 * \file DataSerializerBinary.cs
 * \author Léana Besson
 */
using Model;
using System.Runtime.Serialization;
using System.Xml;

/*!
 * \namespace Persistance
 */
namespace Persistance
{
    /*!
     * \class DataSerializerBinary
     * \brief Contains all the information and functions needed to serialize information in binary format
     */
    public class DataSerializerBinary
    {
        /*!
         * \fn Serializer(string path, Theque theque)
         * \brief Serializes theque information in the file theque.txt
         * \param path string - Path of the file in which the file is to be saved 
         * \param theque Theque - Theque to be serialized
         */
        public static void Serializer(string path, Theque theque)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string txtFile = "theque.txt";

            var serializer = new DataContractSerializer(typeof(Theque), new DataContractSerializerSettings() { PreserveObjectReferences = true });

            using (FileStream stream = File.Create(txtFile))
            {
                using (XmlDictionaryWriter xmlDicoWriter = XmlDictionaryWriter.CreateBinaryWriter(stream))
                {
                    serializer.WriteObject(xmlDicoWriter, theque);
                }
            }
        }

        /*!
         * \fn Deserializer(string path)
         * \brief Deserializes the information contained in theque.txt file if it exists, otherwise it retrieves the information from the stub
         * \param path string - Path of the file in which the file is to be saved 
         * \return Theque - Theque with recovered data 
         */
        public static Theque Deserializer(string path)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            string txtFile = "theque.txt";

            var serializer = new DataContractSerializer(typeof(Theque), new DataContractSerializerSettings() { PreserveObjectReferences = true });

            Theque? theque = new Theque();
            if (File.Exists(txtFile))
            {
                using (FileStream stream = File.OpenRead(txtFile))
                {
                    using (XmlDictionaryReader xmlDicoReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
                    {
                        Theque? thequeOpt = serializer.ReadObject(xmlDicoReader) as Theque;
                        if (thequeOpt != null)
                            theque = thequeOpt;
                        else
                            Console.WriteLine("Theque est null"); 
                    }
                }
            }
            else
            {
                theque = Stub.LoadTheque();
            }
            return theque;
        }
    }
}
