/*!
 * \file New_DetailAnimal.xmal.cs
 * \author L�ana Besson
 */
using Model;
using Persistance;

/*!
 * \namespace Views
 */
namespace Views;

/*!
 * \class New_DetailAnimal
 * \brief Regroups functions for New_DetailAnimal page operation
 */
public partial class New_DetailAnimal : ContentPage
{
    private Animal AnimalSelectione => (App.Current as App).AnimalSelectionner;

    /*! 
     * \fn MainPage()
     * \brief New_DetailAnimal page constructor with component initialization and context binding assignment
     */
    public New_DetailAnimal()
	{
		InitializeComponent();
        (App.Current as App).PageDeSaisie = this;
        InitBinding();
    }

    /*!
     * \fn InitBinding()
     * \brief Initializes binding context
     */
    public void InitBinding()
    {
        BindingContext = AnimalSelectione;
    }

    /*! 
     * \fn Button_OnClick(object sender, EventArgs e)
     * \brief Remove an pet to the theque and opens the pet's data entry page
     * \param sender object - Event emitter information, namely the New_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    public async void Button_OnClick(object sender, EventArgs e)
    {
        (App.Current as App).Theque.SupprimerAnimal((App.Current as App).AnimalSelectionner);
        DataSerializerBinary.Serializer((App.Current as App).SerializationPath, (App.Current as App).Theque);
        await Shell.Current.GoToAsync("//Animaux");
    }

    /*! 
     * \fn Button_OnClick(object sender, EventArgs e)
     * \brief If the animal name is valid, the function serializes the theque and goes to the Animal page
     * \param sender object - Event emitter information, namely the New_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    public async void Validate_OnClick(object sender, EventArgs e)
    {
        if ((App.Current as App).AnimalSelectionner.NomIsValid == true)
        {
            DataSerializerBinary.Serializer((App.Current as App).SerializationPath, (App.Current as App).Theque);
            await Shell.Current.GoToAsync("//Animaux");
        }
    }
}