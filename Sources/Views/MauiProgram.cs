﻿using Microsoft.Extensions.Logging;
using CommunityToolkit.Maui;

namespace Views
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseMauiCommunityToolkit()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("DMSerifDisplay-Regular.ttf", "DMSerifDisplay");
                    fonts.AddFont("Raleway-Light.ttf", "Raleway");
                });

#if DEBUG
		builder.Logging.AddDebug();
#endif

            return builder.Build();
        }
    }
}