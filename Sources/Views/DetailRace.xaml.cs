/*!
 * \file DetailRace.xmal.cs
 * \author L�ana Besson
 * \namespace Views
 */
namespace Views;

/*!
 * \class DetailRace
 * \brief Regroups functions for DetailRace page operation
 */
public partial class DetailRace : ContentPage
{
    /*! 
     * \fn DetailRace()
     * \brief DetailRace page constructor with component initialization and context binding assignment
     */
    public DetailRace()
	{
		InitializeComponent();
		BindingContext = (App.Current as  App).RaceSelectionner;
	}
}