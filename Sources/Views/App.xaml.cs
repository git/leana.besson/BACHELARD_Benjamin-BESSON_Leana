﻿/*! 
 * \file App.xaml.cs
 * \author Léana Besson
 */
using Model;
using Persistance;
using System.ComponentModel;

/*! 
 * \namespace Views
 */
namespace Views
{
    /*! 
     * \class App
     * \brief Regroups functions for App operation
     */
    public partial class App : Application, INotifyPropertyChanged
    {
        public string SerializationPath = FileSystem.Current.AppDataDirectory;
        static private string serializationPath = FileSystem.Current.AppDataDirectory;
        public Theque Theque { get; set; } = DataSerializerBinary.Deserializer(serializationPath);

        public Animal AnimalSelectionner
        {
            get => animalSelectionner;
            set
            {
                animalSelectionner = value;
                OnPropertyChanged(nameof(AnimalSelectionner));
            }
        }
        private Animal animalSelectionner;
        public Espece EspeceSelectionner { get; set; }
        public Race RaceSelectionner { get; set; }
        public New_DetailAnimal PageDeSaisie { get; set; }

        /*! 
         * \fn App()
         * \brief App constructor with component initialization and creation of a new AppShell
         */
        public App()
        {
            InitializeComponent();
            MainPage = new AppShell();
        }
    }
}