﻿/*! 
 * \file AppShell.xaml.cs
 * \author Léana Besson
 * \namespace Views 
 */
namespace Views
{
    /*! 
     * \class Animaux
     * \brief Regroups functions for AppShell operation
     */
    public partial class AppShell : Shell
    {
        /*! 
         * \fn AppShell()
         * \brief Application builder with component initialization
         */
        public AppShell()
        {
            InitializeComponent();
        }
    }
}