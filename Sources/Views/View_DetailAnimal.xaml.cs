/*!
 * \file View_DetailAnimal.xmal.cs
 * \author L�ana Besson
 */
using Model;

/*!
 * \namespace Views
 */
namespace Views;

/*!
 * \class View-DetailAnimal
 * \brief Regroups functions for View_DetailAnimal page operation
 */
public partial class View_DetailAnimal : ContentView
{
    /*! 
     * \fn View_DetailAnimal()
     * \brief View_DetailAnimal page constructor with component initialization and context binding assignment
     */
    public View_DetailAnimal()
	{
		InitializeComponent();
        Picker_especes.BindingContext = (App.Current as App).Theque;
    }

    /*! 
     * \fn EspeceClic(object sender, EventArgs e)
     * \brief Changes the species of the selected pet and makes the race null 
     * \param sender object - Event emitter information, namely the View_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    private void EspeceClic(object sender, EventArgs e)
    {
        (App.Current as App).AnimalSelectionner.Espece = (sender as Picker).SelectedItem as Espece;
        (App.Current as App).AnimalSelectionner.Race = null;
    }

    /*! 
     * \fn RaceClic(object sender, EventArgs e)
     * \brief Modifies the breed of the selected pet
     * \param sender object - Event emitter information, namely the View_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    private void RaceClic(object sender, EventArgs e)
    {
        (App.Current as App).AnimalSelectionner.Race = (sender as Picker).SelectedItem as Race;
    }

    /*! 
     * \fn SexeClick(object sender, EventArgs e)
     * \brief Modifies the gender of the selected pet
     * \param sender object - Event emitter information, namely the View_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    private void SexeClick(object sender, EventArgs e)
    {
        (App.Current as App).AnimalSelectionner.Sexe = (sender as Picker).SelectedItem as string;
    }

    /*! 
     * \fn TakePhoto(object sender, EventArgs e)
     * \brief Opens photo picker, loads photo into AppDataDirectory file and changes image of selected pet
     * \param sender object - Event emitter information, namely the View_DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    public async void TakePhoto(object sender, EventArgs e)
    {
        if (MediaPicker.Default.IsCaptureSupported)
        {
            FileResult photo = await MediaPicker.Default.PickPhotoAsync();

            if (photo != null)
            {
                string localFilePath = Path.Combine(FileSystem.AppDataDirectory, photo.FileName);

                using Stream sourceStream = await photo.OpenReadAsync();
                using FileStream localFileStream = File.OpenWrite(localFilePath);

                await sourceStream.CopyToAsync(localFileStream);

                (App.Current as App).AnimalSelectionner.Image = Path.Combine(FileSystem.AppDataDirectory, photo.FileName);
            }
        }
    }

}