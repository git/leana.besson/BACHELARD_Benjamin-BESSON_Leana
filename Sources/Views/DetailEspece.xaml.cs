/*!
 * \file DetailEspece.xmal.cs
 * \author L�ana Besson
 */
using Model;

/*!
 * \namespece Views
 */
namespace Views;

/*!
 * \class DetailEspece
 * \brief Regroups functions for DetailEspece page operation
 */
public partial class DetailEspece : ContentPage
{
    /*! 
     * \fn DetailEspece()
     * \brief DetailEspece page constructor with component initialization and context binding assignment
     */
    public DetailEspece()
	{
		InitializeComponent();
		BindingContext = (App.Current as App).EspeceSelectionner;
	}

    /*! 
     * \fn OnClick(object sender, ItemTappedEventArgs e)
     * \brief Saves the breed selected by the user and opens a new DetailRace page
     * \param sender object - Event emitter information, namely the DetailEspece.xaml page
     * \param e ItemTappedEventArgs - Information on selected breed
     */
    public async void OnClick(object sender, ItemTappedEventArgs e)
    {
        (App.Current as App).RaceSelectionner = e.Item as Race;
        await Navigation.PushAsync(new DetailRace());
    }
}