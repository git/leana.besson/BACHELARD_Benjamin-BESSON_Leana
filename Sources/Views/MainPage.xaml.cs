﻿/*!
 * \file MainPage.xmal.cs
 * \author Léana Besson
 * \namespace Views
 */
namespace Views
{
    /*!
     * \class MainPage
     * \brief Regroups functions for Especes page operation
     */
    public partial class MainPage : ContentPage
    {
        /*! 
         * \fn MainPage()
         * \brief MainPage page constructor with component initialization and context binding assignment
         */
        public MainPage()
        {
            InitializeComponent();
        }
    }
}