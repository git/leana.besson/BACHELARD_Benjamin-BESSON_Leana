/*! 
 * \file Animal.xaml.cs
 * \author L�ana Besson
 */
using Model;
using Persistance;

/*! 
 * \namespace Views
 */
namespace Views;

/*! 
 * \class DetailAnimal
 * \brief Regroups functions for DetailAnimal page operation
 */
public partial class DetailAnimal : ContentPage
{
    /*!
     * \fn AppShell()
     * \brief Animaux page constructor with component initialization and context binding assignment
     */
    public DetailAnimal()
	{
		InitializeComponent();
		BindingContext = (App.Current as App).AnimalSelectionner;
        if((App.Current as App).AnimalSelectionner.Espece != null)
        {
            if ((App.Current as App).AnimalSelectionner.Race != null)
            {
                PetAdvice.BindingContext = (App.Current as App).AnimalSelectionner.Race;
            }
            else PetAdvice.BindingContext = (App.Current as App).AnimalSelectionner.Espece;
        }
	}

    /*!
     * \fn Button_OnClick(object sender, EventArgs e)
     * \brief Delete the animal from the theque, serialize the theque and go to the Animaux page
     * \param sender object - Event emitter information, namely the DetailAnimal.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    public async void Button_OnClick(object sender, EventArgs e)
    {
        (App.Current as App).Theque.SupprimerAnimal((App.Current as App).AnimalSelectionner);
        DataSerializerBinary.Serializer((App.Current as App).SerializationPath, (App.Current as App).Theque);
        await Shell.Current.GoToAsync("//Animaux");
    }

    /*!
     * \fn OnBackButtonPressed()
     * \brief If the Name is valid the theque is serialized 
     * \return False if the button is pressed and True otherwise
     */
    protected override bool OnBackButtonPressed()
    {
        if((App.Current as App).AnimalSelectionner.NomIsValid == true)
        {
            DataSerializerBinary.Serializer((App.Current as App).SerializationPath, (App.Current as App).Theque);
            return base.OnBackButtonPressed();
        }
        return true;
    }
}