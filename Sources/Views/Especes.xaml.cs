/*!
 * \file Especes.xmal.cs
 * \author L�ana Besson
 */
using Model;

/*!
 * \namespace Views
 */
namespace Views;

/*!
 * \class Especes
 * \brief Regroups functions for Especes page operation
 */
public partial class Especes : ContentPage
{
    /*! 
     * \fn DetailRace()
     * \brief Especes page constructor with component initialization and context binding assignment
     */
    public Especes()
	{
        InitializeComponent();
        BindingContext = (App.Current as App).Theque;
	}

    /*! 
     * \fn OnClick(object sender, ItemTappedEventArgs e)
     * \brief Saves the species selected by the user and opens a new DetailEspece page
     * \param sender object - Event emitter information, namely the Especes.xaml page
     * \param e ItemTappedEventArgs - Information on selected species
     */
    public void OnClick(object sender, ItemTappedEventArgs e)
    { 
        (App.Current as App).EspeceSelectionner = e.Item as Espece;       
        Navigation.PushAsync(new DetailEspece());
    }
}
