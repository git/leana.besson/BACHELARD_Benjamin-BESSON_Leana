/*! 
 * \file Animal.xaml.cs
 * \author L�ana Besson
 */
using Model;

/*! 
 * \namespace Views
 */
namespace Views;

/*! 
 * \class Animaux
 * \brief Regroups functions for the proper operation of the Pets page
 */
public partial class Animaux : ContentPage
{
    /*! 
     * \fn Animaux()
     * \brief Animaux page constructor with component initialization and context binding assignment
     */
    public Animaux()
	{
		InitializeComponent();
        BindingContext = (App.Current as App).Theque;
	}

    /*! 
     * \fn OnClick(object sender, ItemTappedEventArgs e)
     * \brief Saves the pet selected by the user and opens a new DetailAnimal page
     * \param sender object - Event emitter information, namely the Animaux.xaml page
     * \param e ItemTappedEventArgs - Information on selected pet
     */
    public void OnClick(object sender, ItemTappedEventArgs e)
    {
        (App.Current as App).AnimalSelectionner = e.Item as Animal;
        Navigation.PushAsync(new DetailAnimal());
    }

    /*! 
     * \fn Button_OnClick(object sender, EventArgs e)
     * \brief Adds an pet to the theque and opens the pet's data entry page
     * \param sender object - Event emitter information, namely the Animals.xaml page
     * \param e EventArgs - Information related to the clicked button event
     */
    public async void Button_OnClick(object sender, EventArgs e)
	{
        (App.Current as App).AnimalSelectionner = (App.Current as App).Theque.AjouterAnimal();
        (App.Current as App).PageDeSaisie?.InitBinding();
        await Shell.Current.GoToAsync("//New_DetailAnimal");
    }
}

