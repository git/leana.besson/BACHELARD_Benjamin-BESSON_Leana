﻿/*!
 * \file Program.cs
 * \author Léana Besson
 */
using Model;


/*!
 * \namespace MyProject
 */
namespace MyProject;

/*!
 * \class Program
 * \brief Group functions to run functional tests
 */
class Program
{
    /*!
     * \brief Contains pet lists and species lists
     */
    static private Theque Theque { get; set; } = Stub.LoadTheque();

    /*!
     * \fn Main(string[] args)
     * \brief It launches the main menu
     * \param args string[] - 
     */
    static void Main(string[] args)
    {
       
        MenusPrincipal();
    }

    /*!
     * \fn MenusPrincipal()
     * \brief It displays the main menu, the user enters a number and the function launches the selected menu or closes the application
     */
    static private void MenusPrincipal()
    {
        while (true)
        {
            Console.WriteLine("MENUS PRINCIPAL");
            Console.WriteLine("\t1- Les espèces");
            Console.WriteLine("\t2- Vos animaux");
            Console.WriteLine("\t9- Quitter");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while(choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    Console.Clear();
                    MenusEspece();
                    break;
                case 2:
                    Console.Clear();
                    MenusAnimal();
                    break;
                case 9:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn MenusEspece()
     * \brief It displays the species menu, the user enters a number and the function launches the chosen function or returns to the main menu
     */
    static private void MenusEspece()
    {
        while (true)
        {
            Console.WriteLine("LES ESPECES");
            Console.WriteLine("\t1- Afficher les espèces");
            Console.WriteLine("\t2- Sélectionner une espèce");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    Console.Clear();
                    AfficherListeEspece();
                    break;
                case 2:
                    Console.Clear();
                    SelectionnerEspece();
                    break;
                case 9:
                    Console.Clear();
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn AfficherListeEspece()
     * \brief Displays species from the Theque species list, displays name and scientific name 
     */
    static private void AfficherListeEspece()
    {
        Console.WriteLine("LISTE DES ESPECES : ");
        foreach (Espece espece in Theque.ListeEspeces)
        {
            Console.WriteLine("\t" + espece.Nom + " (" + espece.NomScientifique + ")");
        }
        Console.WriteLine("\n");
    }

    /*!
     * \fn SelectionnerEspece()
     * \brief It displays the list of species, the user enters the name of a species and the function asks the question again until the user has entered a species from the list, or -1 to return to the species menu
     */
    static private void SelectionnerEspece()
    {
        string? choix = null;
        while (choix != "-1")
        {
            AfficherListeEspece();

            Console.Write("\n\tEntrer le nom de l'espèce à sélectionner (-1 pour annuler) : ");
            choix = Console.ReadLine();

            if(choix != null)
            {
                Espece? espece = Theque.RechercherEspece(choix);

                if (espece != null)
                {
                    AfficherEspece(espece);
                }
                else Console.WriteLine("\tChoix incorrect\n");
            }
        }
    }

    /*!
     * \fn AfficherEspece(Espece espece)
     * \brief Displays species information, displays a menu, the user enters the number, the chosen function is launched or returns to the species menu
     * \param espece Espece - Species to display
     */
    static private void AfficherEspece(Espece espece)
    {
        Console.WriteLine("\n" + espece.Nom);
        Console.WriteLine("\tNom scientifique : " + espece.NomScientifique);
        Console.WriteLine("\tEspérance de vie : " + espece.EsperanceVie);
        Console.WriteLine("\tPoids moyen : " + espece.PoidsMoyen);
        Console.WriteLine("\tTaille moyenne : " + espece.TailleMoyenne);
        Console.WriteLine("\tComportement : " + espece.Comportement);
        Console.WriteLine("\tSanté : " + espece.Sante);
        Console.WriteLine("\tEducation : " + espece.Education);
        Console.WriteLine("\tEntretien : " + espece.Entretien);
        Console.WriteLine("\tCout : " + espece.Cout);
        Console.WriteLine("\tConseil : " + espece.Conseil);

        AfficherListeRace(espece);

        while (true)
        {
            Console.WriteLine("\n\t1- Sélectionner une race");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    SelectionnerRace(espece);
                    break;
                case 9:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn AfficherListeRace(Espece espece)
     * \brief Displays the list of breeds of the selected species, showing the name and scientific name of the breed
     * \param espece Espece - The species for which the breed list is to be displayed
     */
    static private void AfficherListeRace(Espece espece)
    {
        Console.WriteLine("\nLISTE DES RACES : ");
        if (espece.ListeRaces != null)
        {
            foreach (Race race in espece.ListeRaces)
            {
                Console.WriteLine("\t" + race.Nom + " (" + race.NomScientifique + ")");
            }
            Console.WriteLine("\n");
        }
        else Console.WriteLine("\tAucune race connue.\n");
    }

    /*!
     * \fn SelectionnerRace(Espece espece)
     * \biref It displays the list of breeds of the species, the user enters the name of a breed and the function asks again until the user has entered a breed from the list or -1 to return to the species display
     * \param espece - Espece The species selected by the user for which the breed is to be selected
     */
    static private void SelectionnerRace(Espece espece)
    {
        string? choix = "";
        while (choix != "-1")
        {
            Console.Write("\n\tEntrer le nom de la race à sélectionner (-1 pour annuler) : ");
            choix = Console.ReadLine();

            if (choix != "-1")
            {
                Race? race = espece.RechercherRace(choix);

                if (race != null)
                {
                    AfficherRace(race);
                }
                else Console.WriteLine("\tChoix incorrect\n");
            }
        }
    }

    /*! 
     * \fn AfficherRace(Race race)
     * \brief Displays information on the selected breed
     * \param race Race - The breed to display
     */
    static private void AfficherRace(Race race)
    {
        Console.WriteLine("\n " + race.Nom);
        Console.WriteLine("\tNom scientifique : " + race.NomScientifique);
        Console.WriteLine("\tEspérance de vie : " + race.EsperanceVie);
        Console.WriteLine("\tPoids moyen : " + race.PoidsMoyen);
        Console.WriteLine("\tTaille moyenne : " + race.TailleMoyenne);
        Console.WriteLine("\tComportement : " + race.Comportement);
        Console.WriteLine("\tSante : " + race.Sante);
        Console.WriteLine("\tEducation : " + race.Education);
        Console.WriteLine("\tEntretien : " + race.Entretien);
        Console.WriteLine("\tCout : " + race.Cout);
        Console.WriteLine("\tConseil : " + race.Conseil + "\n\n");
    }

    /*!
     * \fn MenusAnimal()
     * \brief It displays the animal menu, the user enters a number and the function launches the chosen function or returns to the main menu
     */
    static private void MenusAnimal()
    {
        while (true)
        {
            Console.WriteLine("LES ANIMAUX");
            Console.WriteLine("\t1- Afficher les animaux");
            Console.WriteLine("\t2- Ajouter un animal");
            Console.WriteLine("\t3- Sélectionner un animal");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    Console.Clear();
                    AfficherListeAnimaux();
                    break;
                case 2:
                    Console.Clear();
                    Animal animal = Theque.AjouterAnimal();
                    ModifierNom(animal);
                    ModifierAnimal(animal);
                    break;
                case 3:
                    Console.Clear();
                    SelectionnerAnimal();
                    break;
                case 9:
                    Console.Clear();
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn AfficherListeAnimaux()
     * \brief Displays animals from the Theque animal list
     */
    static private void AfficherListeAnimaux()
    {
        Console.WriteLine("VOS ANIMAUX : ");
        foreach (Animal animal in Theque.ListeAnimaux)
        {
            if(animal.Espece != null) Console.WriteLine(animal.Nom + "(" + animal.Espece + ")");
            else Console.WriteLine(animal.Nom);
        }
    }

    /*!
     * \fn SelectionnerAnimal()
     * \brief It displays the list of pets, the user enters the name of a pet and the function asks the question again until the user has entered a pet from the list, or -1 to return to the pet menus
     */
    static private void SelectionnerAnimal()
    {
        string? choix = "";
        while (choix != "-1")
        {
            AfficherListeAnimaux();

            Console.Write("\n\tEntrer le nom de l'animal à sélectionner (-1 pour annuler) : ");
            choix = Console.ReadLine();

            Animal? animal = Theque.RechercherAnimal(choix);

            if (animal != null)
            {
                AfficherAnimal(animal);
            }
            else Console.WriteLine("\tChoix incorrect\n");
        }
    }

    /*!
     * \fn AfficherAnimal(Animal animal)
     * \brief Displays information on the selected animal, displays a menu of pet functions, the user enters the number, the selected function is launched or returns to the animal menu
     * \param animal Animal - Animal selected for display
     */
    static private void AfficherAnimal(Animal animal)
    {
        Console.Clear();
        while (true)
        {
            Console.WriteLine("\n" + animal.Nom);
            if (animal.Espece != null) Console.WriteLine("\tEspece : " + animal.Espece.Nom);
            if (animal.Race != null) Console.WriteLine("\tRace : " + animal.Race.Nom);
            Console.WriteLine("\tDate de naissance : " + animal.DateNaissance);
            Console.WriteLine("\tSexe : " + animal.Sexe);
            Console.WriteLine("\tDate d'adoption : " + animal.DateAdoption);
            Console.WriteLine("\tTaille : " + animal.Taille);
            Console.WriteLine("\tPoids : " + animal.Poids);
            Console.WriteLine("\tAlimentation : " + animal.Alimentation);
            Console.WriteLine("\tPETSITTER : ");
            AfficherEntite(animal.Petsitter);
            Console.WriteLine("\tCHENIL : ");
            AfficherEntite(animal.Chenil);
            Console.WriteLine("\tVETERINAIRE : ");
            AfficherVeterinaire(animal.Veterinaire);
            Console.WriteLine("\tMAGASIN ALIMENTAIRE : ");
            AfficherEntite(animal.MagasinAlimentaire);
            Console.WriteLine("\tREFUGE, ELEVAGE, CHENIL DE PROVENANCE : ");
            AfficherEntite(animal.Provenance);


            Console.WriteLine("\n\t1- Modifier");
            Console.WriteLine("\t2- Supprimer");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    ModifierAnimal(animal);
                    break;
                case 2:
                    Theque.SupprimerAnimal(animal);
                    return;
                case 9:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn AfficherEntite(Entite entite)
     * \brief Displays information on the selected entity
     * \param entite Entite - Selected entity to display
     */
    static private void AfficherEntite(Entite entite)
    {
        Console.WriteLine("\t\tNom : " + entite.Nom);
        Console.WriteLine("\t\tAdresse : " + entite.Adresse + "," + Convert.ToInt32(entite.CodePostal) + " " + entite.Ville);
        Console.WriteLine("\t\tNuméro de téléphone : " + Convert.ToInt32(entite.NumTel));
    }

    /*!
     * \fn AfficherVeterinaire(Veterinaire veterinaire)
     * \brief Displays information about the selected veterinarian
     * \param veterinaire Veterinaire - Veterinarian selected to display
     */
    static private void AfficherVeterinaire(Veterinaire veterinaire)
    {
        Console.WriteLine("\t\tNom : " + veterinaire.Nom);
        Console.WriteLine("\t\tClinique : " + veterinaire.Clinique);
        Console.WriteLine("\t\tAdresse : " + veterinaire.Adresse + "," + Convert.ToInt32(veterinaire.CodePostal) + " " + veterinaire.Ville);
        Console.WriteLine("\t\tNuméro de téléphone : " + Convert.ToInt32(veterinaire.NumTel));
    }

    /*!
     * \fn ModifierAnimal(Animal animal)
     * \brief Displays the pet's menu of items to be modified, the user enters the number of the item to be modified, launches the function of the item to be modified or returns to the pet display
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierAnimal(Animal animal)
    {
        while (true)
        {
            Console.WriteLine("MODIFIER L'ANIMAL ", animal.Nom);
            Console.WriteLine("\t1- Nom");
            Console.WriteLine("\t2- Espece");
            Console.WriteLine("\t3- Race");
            Console.WriteLine("\t4- Date de naissance");
            Console.WriteLine("\t5- Sexe");
            Console.WriteLine("\t6- Date d'adoption");
            Console.WriteLine("\t7- Taille");
            Console.WriteLine("\t8- Poids");
            Console.WriteLine("\t9- Alimentation");
            Console.WriteLine("\t10- Petsitter");
            Console.WriteLine("\t11- Chenil");
            Console.WriteLine("\t12- Vétérinaire");
            Console.WriteLine("\t13- Magasin alimentaire");
            Console.WriteLine("\t14- Refuge, élevage et chenil de provenance");
            Console.WriteLine("\t19- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    ModifierNom(animal);
                    break;
                case 2:
                    ModifierEspece(animal);
                    break;
                case 3:
                    ModifierRace(animal);
                    break;
                case 4:
                    ModifierDateNaissance(animal);
                    break;
                case 5:
                    ModifierSexe(animal);
                    break;
                case 6:
                    ModifierDateAdoption(animal);
                    break;
                case 7:
                    ModifierTaille(animal);
                    break;
                case 8:
                    ModifierPoids(animal);
                    break;
                case 9:
                    ModifierAlimentation(animal);
                    break;
                case 10:
                    ModifierEntite(animal.Petsitter);
                    break;
                case 11:
                    ModifierEntite(animal.Chenil);
                    break;
                case 12:
                    ModifierVeterinaire(animal.Veterinaire);
                    break;
                case 13:
                    ModifierEntite(animal.MagasinAlimentaire);
                    break;
                case 14:
                    ModifierEntite(animal.Provenance);
                    break;
                case 19:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn ModifierNom(Animal animal)
     * \brief The user enters the name of the pet to be modified, checks if it is valid, asks the question again until the name is valid, changes the pet's name
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierNom(Animal animal)
    {
        Console.Write("\tNom : ");
        string nom = Console.ReadLine()??"";

        while(!animal.NomValidate(nom))
        {
            Console.Write("\nNom incorrect. Nom : ");
            nom = Console.ReadLine() ??"";
        }

        animal.Nom = nom;
    }

    /*!
     * \fn ModifierEspece(Animal animal)
     * \brief The user enters the species name of the pet to be modified, checks if the pet exists, asks the question again until the pet written does not exist, then changes the animal species 
     * \param animal Animal - animal Animal - Pet to modify 
     */
    static private void ModifierEspece(Animal animal)
    {
        Console.Write("\tEspèce (appuyer sur entrée pour passer): ");
        string? nomEspece = Console.ReadLine();
        Espece? espece = Theque.RechercherEspece(nomEspece);

        while (nomEspece != null && espece == null)
        {
            Console.Write("\tEspèce inconnue. Espèce : ");
            nomEspece = Console.ReadLine();
            espece = Theque.RechercherEspece(nomEspece);
        }
        animal.Espece = espece;
    }

    /*! 
     * \fn ModifierSexe(Animal animal)
     * \brief The user enters the species name of the pet to be modified, checks if the species exists, asks the question again until the species written does not exist, then changes the pet sexe
     * \param animal Animal - Pet to modify
     */
    static private void ModifierSexe(Animal animal)
    {
        string? sexe = null;
        while (sexe != "Male" && sexe != "Femelle" && sexe != null)
        {
            Console.Write("\tSexe [Male|Femelle] (appuyer sur entrer pour passer) : ");
            sexe = Console.ReadLine();
        }
        animal.Sexe = sexe;
    }

    /*!
     * \fn ModifierTaille(Animal animal)
     * \brief The user enters new size, the function checks whether it is valid and, if it is not valid, modifies the size
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierTaille(Animal animal)
    {
        Console.Write("\tTaille (appuyer sur entrer pour passer) : ");
        string? taille = Console.ReadLine();

        while(taille != null && !Theque.FloatValidate(taille))
        {
            Console.Write("\tTaille incorrect. Taille : ");
            taille = Console.ReadLine();
        }
        animal.Taille = Convert.ToSingle(taille);
    }

    /*!
     * \fn ModifierPoids(Animal animal)
     * \brief The user enters new weight, the function checks whether it is valid and asks the question again if it is not valid, modifying the pet weight
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierPoids(Animal animal)
    {
        Console.Write("\tPoids (appuyer sur entrer pour passer) : ");
        string? poids = Console.ReadLine();

        while (poids != null && !Theque.FloatValidate(poids))
        {
            Console.Write("\tTaille incorrect. Poids : ");
            poids = Console.ReadLine();
        }
        animal.Poids = Convert.ToSingle(poids);
    }

    /*!
     * \fn ModifierAlimentation(Animal animal)
     * \brief User enters new feed, function modifies pet feed
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierAlimentation(Animal animal)
    {
        Console.Write("\tAlimentation (appuyer sur entrer pour passer) : ");
        animal.Alimentation = Console.ReadLine();
    }

    /*!
     * \fn ModifierDateNaissance(Animal animal)
     * \brief The user enters the new date of birth, the function checks that it is valid and asks the question again if it is not, then changes the pet's date of birth
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierDateNaissance(Animal animal)
    {
        Console.Write("\tDate de naissance (appuyer sur entrer pour passer) : ");
        string? dateNaissance = Console.ReadLine();

        while (dateNaissance != null && !Theque.DateTimeValidate(dateNaissance))
        {
            Console.Write("\tTaille incorrect. Date de naissance : ");
            dateNaissance = Console.ReadLine();
        }

        animal.DateNaissance = Convert.ToDateTime(dateNaissance);
    }

    /*!
     * \fn ModifierDateAdoption(Animal animal)
     * \brief The user enters the new adoption date, the function checks that it's valid and asks again if it isn't, then changes the pet's adoption date
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierDateAdoption(Animal animal)
    {
        Console.Write("\tDate d'adoption (appuyer sur entrer pour passer) : ");
        string? dateAdoption = Console.ReadLine();

        while (dateAdoption != null && !Theque.DateTimeValidate(dateAdoption))
        {
            Console.Write("\tTaille incorrect. Date d'adoption : ");
            dateAdoption = Console.ReadLine();
        }

        animal.DateAdoption = Convert.ToDateTime(dateAdoption);

    }

    /*!
     * \fn ModifierRace(Animal animal)
     * \brief The user enters the name of the new breed, the function checks that the breed exists and asks again if it doesn't, then changes the pet's breed.
     * \param animal Animal - Pet to modify 
     */
    static private void ModifierRace(Animal animal)
    {
        if (animal.Espece != null)
        {
            Console.Write("\tRace (appuyer sur entrée pour passer): ");
            string? nomRace = Console.ReadLine();
            Race? race = animal.Espece.RechercherRace(nomRace);

            while (nomRace != null && race == null)
            {
                Console.Write("\tRace inconnue. Race : ");
                nomRace = Console.ReadLine();
                race = animal.Espece.RechercherRace(nomRace);
            }
            animal.Race = race;
        }
        else Console.WriteLine("\tL'animal ne peut pas avoir une race sans espèce");
    }

    /*!
     * \fn ModifierEntite(Entite entite)
     * \brief Displays the elements to be modified in the entity, the user enters a number and the associated function is launched. The function repeats the same actions until the user enters the return number.
     * \param entite Entite - Entity to modify 
     */
    static private void ModifierEntite(Entite entite)
    {
        while (true)
        {
            Console.WriteLine("MODIFIER L'ENTITE ", entite);
            Console.WriteLine("\t1- Nom");
            Console.WriteLine("\t2- Adresse");
            Console.WriteLine("\t3- Code postal");
            Console.WriteLine("\t4- Ville");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    ModifierNomEntite(entite);
                    break;
                case 2:
                    ModifierAdresseEntite(entite);
                    break;
                case 3:
                    ModifierCodePostalEntite(entite);
                    break;
                case 4:
                    ModifierVilleEntite(entite);
                    break;
                case 9:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn ModifierVeterinaire(Veterinaire veterinaire)
     * \brief Affiche les éléments à modifier dans le vétérinaire, l'utilisateur entre un numéro, la fonction associé se lance. La fonction refait les mêmes actions jusqu'à ce que l'utilisateur entre le numéro de retour.
     * \param veterinaire Veterinaire - Veterinarian to modify 
     */
    static private void ModifierVeterinaire(Veterinaire veterinaire)
    {
        while (true)
        {
            Console.WriteLine("MODIFIER L'ENTITE ", veterinaire);
            Console.WriteLine("\t1- Nom");
            Console.WriteLine("\t2- Clinique");
            Console.WriteLine("\t3- Adresse");
            Console.WriteLine("\t4- Code postal");
            Console.WriteLine("\t5- Ville");
            Console.WriteLine("\t9- Retour");

            Console.Write("\n\tEntrer votre choix : ");
            string? choix = Console.ReadLine();
            while (choix == null || !Theque.IntValidate(choix))
            {
                Console.Write("\n\tChoix incorrect. Entrer votre choix : ");
                choix = Console.ReadLine();
            }

            switch (Convert.ToInt32(choix))
            {
                case 1:
                    ModifierNomEntite(veterinaire);
                    break;
                case 2:
                    ModifierClinique(veterinaire);
                    break;
                case 3:
                    ModifierAdresseEntite(veterinaire);
                    break;
                case 4:
                    ModifierCodePostalEntite(veterinaire);
                    break;
                case 5:
                    ModifierVilleEntite(veterinaire);
                    break;
                case 9:
                    return;
                default:
                    Console.WriteLine("\tChoix incorrect\n");
                    break;
            }
        }
    }

    /*!
     * \fn ModifierNomEntite(Entite entite)
     * \brief User enters new entity name and function modifies entity name
     * \param entite Entite - Entity to modify 
     */
    static private void ModifierNomEntite(Entite entite)
    {
        Console.Write("\tNom (appuyer sur entrer pour passer) : ");
        entite.Nom = Console.ReadLine();
    }

    /*!
     * \fn ModifierAdresseEntite(Entite entite)
     * \brief User enters new entity address and function modifies entity address
     * \param entite Entite - Entity to modify 
     */
    static private void ModifierAdresseEntite(Entite entite)
    {
        Console.Write("\tAdresse (appuyer sur entrer pour passer) : ");
        entite.Adresse = Console.ReadLine();
    }

    /*!
     * \fn ModifierCodePostalEntite(Entite entite)
     * \brief The user enters the new zip code, the function checks that it is valid and asks the question again if it is not. It changes the entity's zip code
     * \param entite Entite - Entity to modify 
     */
    static private void ModifierCodePostalEntite(Entite entite)
    {
        Console.Write("\tCode postal (appuyer sur entrer pour passer) : ");
        string? codePostal = Console.ReadLine();

        while(!entite.CodePostalValidate(codePostal)) {
            Console.Write("\tCode postal (appuyer sur entrer pour passer) : ");
            codePostal = Console.ReadLine();
        }

        entite.CodePostal = Convert.ToInt32(codePostal);   
    }

    /*!
     * \fn ModifierVilleEntite(Entite entite)
     * \brief User enters new entity city and function modifies entity city
     * \param entite Entite - Entity to modify 
     */
    static private void ModifierVilleEntite(Entite entite)
    {
        Console.Write("\tVille (appuyer sur entrer pour passer) : ");
        entite.Ville = Console.ReadLine();
    }

    /*!
     * \fn ModifierClinique(Veterinaire veterinaire)
     * \brief The user enters a new veterinary clinic and the function modifies the veterinary clinic. 
     * \param veterinaire Veterinaire - Veterinarian to modify 
     */
    static private void ModifierClinique(Veterinaire veterinaire)
    {
        Console.Write("\tClinique (appuyer sur entrer pour passer) : ");
        veterinaire.Clinique = Console.ReadLine();
    }
}