﻿/*!
 * \file Theque.cs
 * \author Léana Besson
 */
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

/*!
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Theque 
     * \brief Regroups all the species and animals as well as the functions to make it work
     */
    [DataContract(Name = "theque")]
    public class Theque
    {
        [DataMember(Name = "especes")]
        public List<Espece> ListeEspeces { get; set; }

        [DataMember(Name = "animaux")]
        public ObservableCollection<Animal> ListeAnimaux { get; set; } 
        
        /*!
         * \fn Theque()
         * \brief Theque class constructor 
         */
        public Theque() 
        {
            ListeEspeces = new List<Espece>();
            ListeAnimaux = new ObservableCollection<Animal>();
        }

        /*!
         * \fn AjouterAnimal()
         * \brief Add an animal to the animal list
         * \return Animal - Animal add to list
         */
        public Animal AjouterAnimal()
        {
            Animal animal = new Animal();
            ListeAnimaux.Add(animal);
            return animal;
        }

        /*!
         * \fn SupprimerAnimal(Animal animal)
         * \brief Remove animal from animal list
         * \param animal Animal - Animal to remove from list
         */
        public void SupprimerAnimal(Animal animal)
        {
            ListeAnimaux.Remove(animal);
        }

        /*!
         * \fn RechercherAnimal(string? choix)
         * \brief Retrieves each animal from the animal list and checks whether the animal's name matches the name searched for
         * \param choix string? - Name of pet wanted 
         * \return Animal? - Pet wanted or null if not found
         */
        public Animal? RechercherAnimal(string? choix)
        {
            foreach (Animal animal in ListeAnimaux)
            {
                if (animal.Nom == choix)
                {
                    return animal;
                }
            }
            return null;
        }

        /*!
         * \fn RechercherEspece(string? choix)
         * \brief Retrieves each species from the species list and checks whether the species name and the name searched for are identical 
         * \param choix string? - Name of species wanted 
         * \return Espece? - Species wanted or null if not found
         */
        public Espece? RechercherEspece(string? choix)
        {
            foreach (Espece espece in ListeEspeces)
            {
                if (espece.Nom == choix)
                {
                    return espece;
                }
            }
            return null;
        }

        /*!
         * \fn IntValidate(string? response)
         * \brief Checks whether the string is null or convertible to an integer
         * \param response string? - Character string to check
         * \return bool - True if null or convertible and False if not 
         */
        public static bool IntValidate(string? response)
        {
            if (response == null) 
                return true;
            else
            {
                try
                {
                    int number = Convert.ToInt32(response);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        /*!
         * \fn FloatValidate(string? response)
         * \brief Checks whether the string is null or convertible to a float
         * \param response string? - Character string to check
         * \return bool - True if null or convertible and False if not 
         */
        public static bool FloatValidate(string? response)
        {
            if (response == null) 
                return true;
            else
            {
                try
                {
                    float numFloat = Convert.ToSingle(response);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        /*!
         * \fn DateTimeValidate(string? response)
         * \brief Checks whether the string is null or convertible to a DateTime
         * \param response string? - Character string to check
         * \return True if null or convertible and False if not 
         */
        public static bool DateTimeValidate(string? response)
        {
            if (response == null) 
                return true;
            else
            {
                try
                {
                    DateTime date = Convert.ToDateTime(response);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
