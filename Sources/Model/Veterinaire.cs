﻿/*!
 * \file Veterinaire.cs
 * \author Léana Besson
 */
using System.Runtime.Serialization;

/*!
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Veterinaire
     * \brief Brings together all veterinary-related information and functions
     */
    [DataContract(Name = "veterinaire")]
    public class Veterinaire : Entite
    {
        [DataMember(Name = "clinique")]
        public string? Clinique
        {
            get => clinique;
            set
            {
                if(clinique == value) 
                   return; 
                clinique = value;
                OnPropertyChanged(nameof(Clinique));
            }
        }
        private string? clinique;
    }
}
