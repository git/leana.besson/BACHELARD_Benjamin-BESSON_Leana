﻿/*!
 * \file Entite.cs
 * \author Léana Besson
 */
using System.ComponentModel;
using System.Runtime.Serialization;

/*!
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Entite
     * \brief Regroups all properties and functions related to the entity
     */
    [DataContract(Name = "entite")]
    public class Entite : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        [DataMember(Name = "nom")]
        public string? Nom
        {
            get => nom;
            set
            {
                if (nom == value)
                    return;
                nom = value;
                OnPropertyChanged(nameof(Nom));
            }
        }
        private string? nom;

        [DataMember(Name = "adresse")]
        public string? Adresse
        {
            get => adresse;
            set
            {
                if (adresse == value)
                    return;
                adresse = value;
                OnPropertyChanged(nameof(Adresse));
            }
        }
        private string? adresse;

        [DataMember(Name = "codePostal")]
        public int? CodePostal
        {
            get => codePostal;
            set
            {
                if (codePostal == value)
                    return;
                codePostal = value;
                OnPropertyChanged(nameof(CodePostal));
            }
        }
        private int? codePostal;

        [DataMember(Name = "ville")]
        public string? Ville
        {
            get => ville;
            set
            {
                if (ville == value)
                    return;
                ville = value;
                OnPropertyChanged(nameof(Ville));
            }
        }
        private string? ville;

        [DataMember(Name = "numTel")]
        public int? NumTel
        {
            get => numTel;
            set
            {
                if(numTel == value)
                    return;
                numTel = value;
                OnPropertyChanged(nameof(NumTel));
            }
        }
        private int? numTel;

        /*!
         * \fn Entite()
         * \brief Entite class constructor
         */
        public Entite()
        {
            Nom = "";
            Adresse = "";
            CodePostal = null;
            Ville = "";
            NumTel = null;
        }

        /*!
         * \fn OnPropertyChanged(string propertyName)
         * \brief The function checks whether the property has been modified
         * \param propertyName string - Property name modify
         */
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /*!
         * \fn CodePostalValidate(string? codePostal)
         * \brief Check that the postal code is non-zero and an integer between 10000 and 99999
         * \param codePostal string? - Postal code of entity to be verified
         * \return bool - Boolean which is false when the Postal code is invalid and true if it is valid
         */
        public bool CodePostalValidate(string? codePostal)
        {
            if ((codePostal != null && !Theque.IntValidate(codePostal)) || (codePostal != null && Theque.IntValidate(codePostal) && Convert.ToInt32(codePostal) < 10000 && Convert.ToInt32(codePostal) > 99999)) return false;
            return true;
        }
    }
}
