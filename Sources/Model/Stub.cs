﻿/*!
 * \file Stub.cs
 * \author Léana Besson
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Stub
     * \brief Includes all functions for retrieving data for the application
     */
    public class Stub
    {
        /*!
         * \fn LoadEspecetheque()
         * \brief Created a species list and filled it with data 
         * \return List<Espece> - Species list with application data
         */
        public static List<Espece> LoadEspecetheque()
        {
            List<Espece> ListeEspeces = new List<Espece>();
            List<Race> Races = new List<Race>();
            Races.Add(new("Abyssin", "", "", "", "", "", "", "", "", "", "Conseil Abyssin"));
            Races.Add(new("American curl"));

            ListeEspeces.Add(new(new List<Race>(), "Chien", "Canis lupus familiaris", "chien.jpg"));
            ListeEspeces.Add(new(Races, "Chat", "Felis catus", "chat.jpg", "15 à 20 ans", "15 à 20 kg", "10 à 15 cm", "Les chats ont un comportement très solitaire", "Les chats ont une bonne santé", "Les chats s'éduque assez facilement", "Il faut changé leur caisse mais il se nettoie seul, sauf les chatons", "Vétérinaire, alimentation adapté, jouet", "Un conseil pour un chat"));
            ListeEspeces.Add(new(new List<Race>(), "Hamster", "Cricetinae"));
            ListeEspeces.Add(new(new List<Race>(), "Lapin", "Oryctolagus cuniculus"));

            return ListeEspeces;
        }

        /*!
         * \fn LoadTheque()
         * \brief Create a theque and fill it with data 
         * \return Theque - Theque with application data
         */
        public static Theque LoadTheque()
        {
            Theque theque = new Theque();

            theque.ListeEspeces = LoadEspecetheque();

            return theque;
        }
    }
}

