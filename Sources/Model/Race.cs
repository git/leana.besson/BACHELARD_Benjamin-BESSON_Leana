﻿/*!
 * \file Race.cs
 * \author Léana Besson
 */
using System.Runtime.Serialization;

/*!
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Race
     * \brief Regroups all properties and functions related to the breed
     */
    [DataContract(Name = "race")]
    public class Race
    {
        [DataMember(Name = "nom")]
        public string Nom { get; set; }

        [DataMember(Name = "scientique")]
        public string NomScientifique { get; set; }

        [DataMember(Name = "esperance")]
        public string EsperanceVie { get; set; }

        [DataMember(Name = "poids")]
        public string PoidsMoyen { get; set; }

        [DataMember(Name = "taille")]
        public string TailleMoyenne { get; set; }

        [DataMember(Name = "comportement")]
        public string Comportement { get; set; }

        [DataMember(Name = "sante")]
        public string Sante { get; set; }

        [DataMember(Name = "education")]
        public string Education { get; set; }

        [DataMember(Name = "entretien")]
        public string Entretien { get; set; }

        [DataMember(Name = "cout")]
        public string Cout { get; set; }

        [DataMember(Name = "conseil")]
        public string Conseil { get; set; }

        [DataMember(Name = "image")]
        public string? Image { get; set; }

        /*! 
         * \fn Race(string nom, string nomScientifique, string esperanceVie, string poidsMoyen, string tailleMoyenne, string comportement, string sante, string education, string entretien, string cout, string conseil, string? image = null)
         * \brief Race class constructor
         */
        public Race(string nom, string nomScientifique= "Inconnu", string esperanceVie = "Inconnue", string poidsMoyen = "Inconnu", string tailleMoyenne = "Inconnue", string comportement = "Inconnu", string sante = "Inconnue", string education = "Inconnue", string entretien = "Inconnu", string cout = "Inconnu", string conseil = "Inconnu", string? image = null)
        {
            Nom = nom;
            NomScientifique = nomScientifique;
            EsperanceVie = esperanceVie;
            PoidsMoyen = poidsMoyen;
            TailleMoyenne = tailleMoyenne;
            Comportement = comportement;
            Sante = sante;
            Education = education;
            Entretien = entretien;
            Cout = cout;
            Conseil = conseil;
            Image = image;
        }

        /*!
         * \fn ToString()
         * \brief Element to display 
         * \return string - Element to display 
         */
        public override string ToString()
        {
            return Nom;
        }
    }
}
