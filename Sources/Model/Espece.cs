﻿/*!
 * \file Espece.cs
 * \author Léana Besson
 */
using System.Runtime.Serialization;

/*!
 * \namespace Model
 */
namespace Model
{
    /*!
     * \class Espece
     * \brief Regroups all properties and functions related to the species
     */
    [DataContract(Name = "espece")]
    public class Espece 
    {
        [DataMember(Name = "nom")]
        public string Nom { get; set; }

        [DataMember(Name = "scientifique")]
        public string NomScientifique { get; set; }

        [DataMember(Name = "image")]
        public string Image { get; set; }

        [DataMember(Name = "esperance")]
        public string EsperanceVie { get; set; }

        [DataMember(Name = "poids")]
        public string PoidsMoyen { get; set; }

        [DataMember(Name = "taille")]
        public string TailleMoyenne { get; set; }

        [DataMember(Name = "races")]
        public List<Race> ListeRaces { get; set; } = new List<Race>();

        [DataMember(Name = "comportement")]
        public string Comportement { get; set; }

        [DataMember(Name = "sante")]
        public string Sante { get; set; }

        [DataMember(Name = "education")]
        public string Education { get; set; }

        [DataMember(Name = "entretien")]
        public string Entretien { get; set; }

        [DataMember(Name = "cout")]
        public string Cout { get; set; }

        [DataMember(Name = "conseil")]
        public string Conseil { get; set; }

        /*! 
         * \fn Espece(string nom = "", string nomScientifique = "", string image = "", string esperanceVie = "", string poidsMoyen = "", string tailleMoyenne = "", List<Race>? races = null, string comportement = "", string sante = "", string education = "", string entretien = "", string cout = "", string conseil = "")
         * \brief Espece class constructor
         */
        public Espece(List<Race> races, string nom = "", string nomScientifique = "", string image = "", string esperanceVie = "", string poidsMoyen = "", string tailleMoyenne = "", string comportement = "", string sante = "", string education = "", string entretien = "", string cout = "", string conseil = "")
        { 
            Nom = nom;
            NomScientifique = nomScientifique;
            Image = image;
            EsperanceVie = esperanceVie;
            PoidsMoyen = poidsMoyen;
            TailleMoyenne = tailleMoyenne;
            ListeRaces = races;
            Comportement = comportement;
            Sante = sante;
            Education = education;
            Entretien = entretien;
            Cout = cout;
            Conseil = conseil;
        }

        /*!
         * \fn ToString()
         * \brief Element to display 
         * \return string - Element to display 
         */
        public override string ToString()
        {
            return Nom;
        }

        /*!
         * \fn RechercherRace(string? choix)
         * \brief Retrieves each item in the species list and checks whether the breed name and the name entered by the user are identical
         * \param choix string? - Name of breed to search for
         * \return Race? - A breed to search if it exists or null
         */
        public Race? RechercherRace(string? choix)
        {
            if (ListeRaces != null && choix != "")
            {
                foreach (Race race in ListeRaces)
                {
                    if (race.Nom == choix)
                    {
                        return race;
                    }
                }
                Console.WriteLine("\n");
            }
            return null;
        }
    }
}
