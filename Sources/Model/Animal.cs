﻿/*! 
 * \file Animal.cs
 * \author Léana Besson
 */
using System.ComponentModel;
using System.Runtime.Serialization;

/*! 
 * \namespace Model
 */
namespace Model
{
    /*! 
     * \class Animal
     * \brief Regroups all properties and functions related to the pet
     */
    [DataContract(Name = "animal")]
    public class Animal : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        [DataMember(Name = "nom")]
        public string? Nom
        {
            get => nom;
            set
            {
                if (nom == value)
                    return;
                nom = value;
                NomIsValid = NomValidate(nom);
                OnPropertyChanged(nameof(Nom));
            }
        }
        private string? nom;

        [DataMember(Name = "nomValid")]
        public bool NomIsValid
        {
            get => nomIsValid;
            set
            {
                if (nomIsValid == value)
                    return;
                nomIsValid = value;
                OnPropertyChanged(nameof(NomIsValid));
            }
        }
        private bool nomIsValid;

        [DataMember(Name = "naissance")]

        public DateTime? DateNaissance
        {
            get => dateNaissance;
            set
            {
                if (dateNaissance == value)
                    return;
                dateNaissance = value;
                OnPropertyChanged(nameof(DateNaissance));
            }
        }
        private DateTime? dateNaissance;

        [DataMember(Name = "sexe")]
        public string? Sexe 
        {
            get => sexe;
            set { 
                if (sexe == value) 
                    return; 
                sexe = value;
                OnPropertyChanged(nameof(Sexe));
            }
        }
        private string? sexe;

        [DataMember(Name = "adoption")]

        public DateTime? DateAdoption
        {
            get => dateAdoption;
            set
            {
                if (dateAdoption == value)
                    return;
                dateAdoption = value;
                OnPropertyChanged(nameof(DateAdoption));
            }
        }
        private DateTime? dateAdoption;

        [DataMember(Name = "taille")]
        public float? Taille
        {
            get => taille;
            set
            {
                if (taille == value)
                    return;
                taille = value;
                OnPropertyChanged(nameof(Taille));
            }
        }
        private float? taille;

        [DataMember(Name = "poids")]
        public float? Poids
        {
            get => poids;
            set
            {
                if (poids == value)
                    return;
                poids = value;
                OnPropertyChanged(nameof(Poids));
            }
        }
        private float? poids;

        [DataMember(Name = "alimentation")]
        public string? Alimentation
        {
            get => alimentation;
            set
            {
                if (alimentation == value)
                    return;
                alimentation = value;
                OnPropertyChanged(nameof(Alimentation));
            }
        }
        private string? alimentation;

        [DataMember(Name = "espece")]
        public Espece? Espece
        {
            get => espece;
            set 
            {
                if (espece == value) 
                    return;
                espece = value;
                OnPropertyChanged(nameof(Espece));
            }
        }
        private Espece? espece;

        [DataMember(Name = "race")]
        public Race? Race
        {
            get => race;
            set
            {
                if(race == value) 
                    return;
                race = value;
                OnPropertyChanged(nameof(Race));
            }
        }
        private Race? race;

        [DataMember(Name = "veterinaire")]
        public Veterinaire Veterinaire
        {
            get => veterinaire;
            set
            {
                if(veterinaire == value)
                    return;
                veterinaire = value;
                OnPropertyChanged(nameof(Veterinaire));
            }
        }
        private Veterinaire veterinaire = new Veterinaire();

        [DataMember(Name = "chenil")]
        public Entite Chenil
        {
            get => chenil;
            set
            {
                if (chenil == value)
                    return;
                chenil = value;
                OnPropertyChanged(nameof(Chenil));
            }
        }
        private Entite chenil = new Entite();

        [DataMember(Name = "magasin")]
        public Entite MagasinAlimentaire
        {
            get => magasinAlimentaire;
            set
            {
                if (magasinAlimentaire == value)
                    return;
                magasinAlimentaire = value;
                OnPropertyChanged(nameof(MagasinAlimentaire));
            }
        }
        private Entite magasinAlimentaire = new Entite();

        [DataMember(Name = "provenance")]
        public Entite Provenance
        {
            get => provenance;
            set
            {
                if (provenance == value)
                    return;
                provenance = value;
                OnPropertyChanged(nameof(Petsitter));
            }
        }
        private Entite provenance = new Entite();

        [DataMember(Name = "petsitter")]
        public Entite Petsitter
        {
            get => petsitter;
            set
            {
                if (petsitter == value)
                    return;
                petsitter = value;
                OnPropertyChanged(nameof(Petsitter));
            }
        }
        private Entite petsitter = new Entite() ;

        [DataMember(Name = "image")]
        public string? Image
        {
            get => image;
            set
            {
                if (image == value)
                    return;
                image = value;
                OnPropertyChanged(nameof(Image));
            }
        }
        private string? image;

        /*!
         * \fn Animal()
         * \brief Animal class constructor
         */
        public Animal()
        { 
            Nom = "";
            NomIsValid = false;
            DateNaissance = DateTime.MinValue;
            Sexe = "";
            DateAdoption = DateTime.MinValue;
            Taille = null;
            Poids = null;
            Alimentation = "";
            Espece = null;
            Race = null;
            Image = "";
        }

        /*!
         * \fn OnPropertyChanged(string propertyName)
         * \brief The function checks whether the property has been modified
         * \param propertyName string - Property name modify
         */
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /*! 
         * \fn NomValidate(string? nom)
         * \brief The function checks if the name is not null, if it has more than 0 letters and if it doesn't start with an empty space
         * \param nom string? - Pet name to be verified
         *  \return bool - Boolean which is false when the name is invalid and true if it is valid
         */
        public bool NomValidate(string? nom)
        {
            if (nom == null || nom.Length <= 0 || nom.StartsWith(" ")) return false;
            return true;
        }
    }
}
